/* สําหรับเชื่อต่อ Route */
const { Router } = require("express");
const { check } = require("express-validator/check");
const { validator } = require("../../middleware/validator");
const router = Router();
const AuthController = require("./AuthController");

router.post("/login", AuthController.login);
router.get("/logout", AuthController.logout);
router.post("/profile", AuthController.profile);
router.post("/re_password", AuthController.re_password);

router.get("/alldata_user", AuthController.alldata_user);
router.post("/insert_user", AuthController.insert_user);
router.post("/delete_user", AuthController.delete_user);
router.post("/update_user", AuthController.update_user);
router.post("/update_user_pagehome", AuthController.update_user_pagehome);

router.get("/alldata_company", AuthController.alldata_company);
router.post("/insert_company", AuthController.insert_company);
router.post("/update_company", AuthController.update_company);
router.post("/delete_company", AuthController.delete_company);
router.post("/sum_maxday", AuthController.sum_maxday);
router.post("/use_the_leave", AuthController.use_the_leave);
router.get("/request", AuthController.requset);

router.get("/alldata_leave_request", AuthController.alldata_leave_request);
router.post("/approve_status",AuthController.approve_status);
router.post("/reject_status",AuthController.reject_status);
router.get("/alldata_flexible_benefit",AuthController.alldata_flexible_benefit);
router.post("/approve_statusBen",AuthController.approve_statusBen);
router.post("/reject_statusBen",AuthController.reject_statusBen);

router.get("/alldata_work",AuthController.alldata_work);
router.post("/checkin",AuthController.checkin);
router.post("/inbreak",AuthController.inbreak);
router.post("/outbreak",AuthController.outbreak);
router.post("/checkout",AuthController.checkout);
router.post("/chartw",AuthController.chartw)

router.get("/alldata_project",AuthController.alldata_project);
router.post("/insert_project",AuthController.insert_project);
router.post("/delete_project",AuthController.delete_project);
router.post("/update_project",AuthController.update_project);
router.post("/chartp",AuthController.chartp)


router.post("/create_my_leave_request",AuthController.create_my_leave_request);
router.get("/show_my_leave_request",AuthController.show_my_leave_request)
router.post("/insert_my_leave_request",AuthController.insert_my_leave_request)

router.post("/create_my_flexible_request",AuthController.create_my_flexible_request);
router.get("/show_my_flexible_request",AuthController.show_my_flexible_request)
router.post("/insert_my_flexible_benefit",AuthController.insert_my_flexible_benefit)
router.get("/show_img/:benID",AuthController.show_img)
router.get("/show_image",AuthController.show_image)

router.post("/report_leave",AuthController.report_leave)

router.post("/report_flexible",AuthController.report_flexible)

router.get("/show_quest",AuthController.show_quest)

module.exports = router;
