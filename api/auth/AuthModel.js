/* สําหรับเชื่อมต่อ Database */
const knex = require("../../knex");
const moment = require("moment")

class AuthModel {
  selectPassword(username) {
    return knex("employee")
      .first("password", "empID")
      .where({ userName: username });
  }
  selectProfile(empID) {
    return knex("employee as e1")
      .first([
        "e1.empID",
        "e1.emp_name",
        "e1.emp_last",
        "e1.userName",
        "rol_name",
        "e1.position",
        "e1.address",
        "e1.tel",
        "e1.email as empEmail",
        "pro_name",
        "e1.starting_date",
        "e1.user_create_id",
        "comname",
        "comID",
        "e2.email as adminEmail"
      ])
      .innerJoin("role", "e1.role", "role.rolID")
      .innerJoin("pro_period", "e1.pro_period", "pro_period.proID")
      .innerJoin("employee as e2", "e1.user_create_id", "e2.empID")
      .innerJoin("company", "e1.company", "company.comID")
      .where({ "e1.empID": empID });
  }
  getEmpYear(empID) {
    return knex.raw(`SELECT floor((datediff(now(),starting_date))/365) as years,
    floor(((datediff(now(),starting_date))%365)/30) as months,
    ((datediff(now(),starting_date))%365)%30 as days
    FROM hr_web.employee WHERE empID = ${empID}`);
  }
  con_email(email) {
    return knex("employee")
      .first("email")
      .where({ email: email });
  }
  re_password(en_password, email) {
    return knex("employee")
      .update({ password: en_password })
      .where({ email: email });
  }
  con_username(userName) {
    return knex("employee")
      .first("userName")
      .where({ userName: userName });
  }
  insert_user(
    emp_name,
    emp_last,
    userName,
    role,
    email,
    pro_period,
    starting_date,
    user_create_id,
    company,
    position,
    address,
    tel
  ) {
    return knex("employee").insert({
      emp_name,
      emp_last,
      userName,
      role,
      email,
      pro_period,
      starting_date,
      user_create_id,
      company,
      position,
      address,
      tel
    });
  }
  alldata_user() {
    return knex("employee as e1")
      .select([
        "e1.empID",
        "e1.email as email",
        "e1.starting_date",
        "e1.userName",
        "rol_name",
        "pro_name",
        "comname",
        "e1.user_create_id",
        "e2.email as admin_email"
      ])
      .innerJoin("role", "e1.role", "role.rolID")
      .innerJoin("pro_period", "e1.pro_period", "pro_period.proID")
      .innerJoin("employee as e2", "e1.user_create_id", "e2.empID")
      .innerJoin("company", "e1.company", "company.comID")
      .columns(knex.raw("concat(e1.emp_name ,' ', e1.emp_last) as full_name"))
      .groupBy("empID")
      .orderBy("e1.empID", "desc");
  }
  update_user({ empID, emp_name, emp_last, pro_period, email, role }) {
    // console.log(empID, emp_name, emp_last, pro_period, email, role);
    return knex("employee")
      .update({
        emp_name,
        emp_last,
        pro_period,
        email,
        role
      })
      .where("empID", empID);
  }
  update_user_pagehome(empID, address, emp_name, emp_last, email, tel) {
    // console.log(empID, address, emp_name, emp_last, email, tel);
    return knex("employee")
      .update({
        emp_name,
        emp_last,
        address,
        tel,
        email
      })
      .where("empID", empID);
  }
  delete_user(empID) {
    return knex("employee")
      .whereIn("empID", empID)
      .del();
  }
  alldata_company() {
    return knex("company")
      .select(["comID", "comname", "time_in", "time_out"])
      .leftJoin("employee", "comID", "company")
      .column(knex.raw("count(empID) as countcompany"))
      .groupBy("comID");
  }
  insert_company(comname, time_in, time_out) {
    return knex("company").insert({ comname, time_in, time_out });
  }
  update_company({ comID, comname, time_in, time_out }) {
    // console.log(comID, comname, time_in, time_out);
    return knex("company")
      .update({
        comname,
        time_in,
        time_out
      })
      .where("comID", comID);
  }
  delete_company(comID) {
    return knex("company")
      .whereIn("comID", comID)
      .del();
  }
  sum_maxday(IDcompany) {
    return knex("lea_type")
      .select(["max_day"])
      .where("IDcompany", IDcompany);
  }
  use_leave_day({ empID, lea_day }) {
    // console.log(empID, lea_day);
    return knex("leave_request")
      .column(knex.raw("sum(datediff(date_end,date_start)+1) as sum_date"))
      .where({ empID: empID, lea_day: lea_day });
  }
  use_leave_time({ empID, lea_day }) {
    return knex("leave_request")
      .column(knex.raw("timediff(date_end,date_start) as sum_time"))
      .where({ empID: empID, lea_day: lea_day });
  }
  request_wait({ status }) {
    return knex("leave_request")
      .column(knex.raw("count(leaID) as waits"))
      .where("status", status);
  }
  request_approve({ status }) {
    return knex("leave_request")
      .column(knex.raw("count(leaID) as approve"))
      .where("status", status);
  }
  request_reject({ status }) {
    return knex("leave_request")
      .column(knex.raw("count(leaID) as reject"))
      .where("status", status);
  }




  alldata_leave_request() {
    return knex("leave_request")

      .select(
        "leaID",
        "employee.emp_name",
        "employee.emp_last",
        "lea_type.leaName_type",
        "lea_day.leaName_day",
        "date_start",
        "date_end",
        "lea_reason",
        "status.sta_name",
        "date_created",
        "comname"
      )
      // .from("leave_request")
      .leftJoin("lea_type", "leave_request.lea_type", "lea_type.leaID_type")
      .leftJoin("lea_day", "leave_request.lea_day", "lea_day.leaID_day")
      .leftJoin("status", "leave_request.status", "status.staID")
      .leftJoin("employee", "leave_request.empID", "employee.empID")
      .leftJoin("company", "employee.company", "company.comID")
      .columns(knex.raw("concat(employee.emp_name ,' ', employee.emp_last) as full_name"))
      .columns(knex.raw("datediff(leave_request.date_end,leave_request.date_start)+1 as total_day"))

      .columns(knex.raw("timediff(leave_request.date_end,leave_request.date_start)/10000 as time_day"))
      // .column(knex.raw("datediff(hour, '2017/08/25 07:00:00', '2017/08/25 12:45:00') AS DateDiff"))
      // .columns(knex.raw("datediff(hour,leave_request.date_start,leave_request.date_end) as total_hour"))
      .orderBy("leave_request.leaID", "desc");
  }

  approve_status(leaID) {
    return knex("leave_request")
      .where("leaID", leaID)
      .update({
        status: '2'
      })
  }

  reject_status(leaID) {
    return knex("leave_request")
      .where("leaID", leaID)
      .update({
        status: '3'
      })
  }

  alldata_flexible_benefit() {
    return knex("benefit")

      .select(
        "benID",
        "employee.emp_name",
        "employee.emp_last",
        "company.comname",
        "ben_type.benName_type",
        "ben_date_request",
        "ben_date_receipt",
        "amount",
        "status.sta_name",
        "employee.userName",
        "flexible_created",
        "employee.email"
      )
      .leftJoin("employee", "benefit.empID", "employee.empID")
      .leftJoin("company", "employee.company", "company.comID")
      .leftJoin("ben_type", "benefit.ben_type", "ben_type.benID_type")
      .leftJoin("status", "benefit.status", "status.staID")
      .columns(knex.raw("concat(employee.emp_name ,' ', employee.emp_last) as full_name"))
      .orderBy("benefit.benID", "desc");

  }

  approve_statusBen(benID) {
    return knex("benefit")
      .where("benID", benID)
      .update({
        status: '2'
      })
  }

  reject_statusBen(benID) {
    return knex("benefit")
      .where("benID", benID)
      .update({
        status: '3'
      })
  }

  alldata_work() {
    return knex("checkin")
      .select(
        "checkin_id",
        "date",
        "date_in",
        "time_in_break",
        "time_out_break",
        "date_out",
        "employee.emp_name",
        "employee.emp_last",
        "company.comname"

      )
      .leftJoin("employee", "checkin.user_id", "employee.empID")
      .leftJoin("company", "checkin.id_company", "company.comID")
      .columns(knex.raw("concat(employee.emp_name ,' ', employee.emp_last) as full_name"))
      .orderBy("checkin.checkin_id", "desc");
  }

  checkin(
    { empID,
      company }
  ) {
    return knex("checkin")
      .insert({
        date: moment().format('YYYY-MM-DD'),
        date_in: moment().format('HH:mm:ss'),
        time_in_break: moment().format('00:00:00'),
        time_out_break: moment().format('00:00:00'),
        date_out: moment().format('00:00:00'),
        user_id: empID,
        id_company: company,
      });
  }
  check_undefined({ empID }) {
    console.log("B", empID);

    return knex("checkin")
      .where("user_id", empID)
      .andWhere("date", moment().format("YYYY-MM-DD"))
      .select(
        "*"
      )
  }
  select_for_checkin({ empID }) {
    console.log("B", empID);

    return knex("employee")
      .where("empID", empID)
      .select(
        "empID",
        "company",
      )
  }

  inbreak({
    checkin_id,
    user_id,
    date,
    date_in
  }) {
    console.log("CID", checkin_id);

    return knex("checkin")
      .where("checkin_id", checkin_id)
      .update({
        time_in_break: moment().format("HH:mm:ss")
      })
  }

  outbreak({
    checkin_id,
    user_id,
    date,
    date_in
  }) {
    console.log("CID", checkin_id);

    return knex("checkin")
      .where("checkin_id", checkin_id)
      .update({
        time_out_break: moment().format("HH:mm:ss")
      })
  }

  checkout({
    checkin_id,
    user_id,
    date,
    date_in
  }) {
    console.log("CID", checkin_id);

    return knex("checkin")
      .where("checkin_id", checkin_id)
      .update({
        date_out: moment().format("HH:mm:ss")
      })
  }

  monthchart({
    empID,
    year,
    month
  }) {
    console.log("Month Chart");
    // console.log("empID", empID);
    // console.log("month", month);
    // console.log("year", year + "-" + month + "%");
    return knex("checkin")
      .select(
        "checkin_id",
        "user_id",
        "date",
        "date_in",
        "time_in_break",
        "time_out_break",
        "date_out"
      )
      .where("user_id", empID)
      .andWhere("date", "like", year + "-" + month + "%")
      .column(knex.raw("timediff(date_out,date_in) as total_work"))
      .column(knex.raw("HOUR(timediff(date_out,date_in)) as Hour"))
      .column(knex.raw("HOUR(timediff(timediff(date_out,date_in),timediff(time_out_break,time_in_break))) as WorkHour"))
      .column(knex.raw("HOUR(timediff(time_out_break,time_in_break)) as BreakHour"))
      .column(knex.raw("timediff(time_out_break,time_in_break) as total_break"))
      // .column(knex.raw("timediff(total_work,total_break) as work_time"))
      .orderBy("checkin_id", "desc");
  }

  yearchart({
    empID,
    year
  }) {
    console.log("Year Chart");

    return knex("checkin")
      .select(
        "checkin_id",
        "user_id",
        "date",
        "date_in",
        "time_in_break",
        "time_out_break",
        "date_out"
      )
      .where("user_id", empID)
      .andWhere("date", "like", year + "%")
      .column(knex.raw("timediff(date_out,date_in) as total_work"))
      .column(knex.raw("HOUR(timediff(date_out,date_in)) as Hour"))
      .column(knex.raw("HOUR(timediff(timediff(date_out,date_in),timediff(time_out_break,time_in_break))) as WorkHour"))
      .column(knex.raw("HOUR(timediff(time_out_break,time_in_break)) as BreakHour"))
      .column(knex.raw("timediff(time_out_break,time_in_break) as total_break"))
      // .column(knex.raw("timediff(total_work,total_break) as work_time"))

      .orderBy("checkin_id", "desc");


  }
  alldata_project() {
    return knex("project")
      .select(
        "project_id",
        "project_name",
        "project_start",
        "project_end",
        "company_id",
        "user_create",
        "employee.emp_name",
        "employee.emp_last",
        "company.comname"

      )
      .leftJoin("employee", "project.user_create", "employee.empID")
      .leftJoin("company", "project.company_id", "company.comID")
      .columns(knex.raw("concat(employee.emp_name ,' ', employee.emp_last) as full_name"))
      .orderBy("project.project_id", "desc");
  }
  insert_project({
    project_name,
    project_start,
    project_end,
    empID,
    id_company
  }) {
    console.log("CC", id_company);

    return knex("project")
      .insert({
        project_name: project_name,
        project_start: project_start,
        project_end: project_end,
        company_id: id_company,
        user_create: empID
      });
  }

  select_company({
    empID
  }) {
    console.log("A", empID);

    return knex("employee")
      .select(
        "company"
      )
      .where("empID", empID)
  }

  delete_project({
    project_id
  }) {
    console.log(project_id);

    return knex("project")
      // .whereIn("project_id", [11, 12, 13])
      .whereIn("project_id", project_id)
      .del();
  }
  update_project({
    project_id,
    project_name
  }) {
    return knex("project")
      .update({
        project_name,
      })
      .where("project_id", project_id);
  }

  chart_project({
    project_id,
  }) {
    return knex("job")
      .select(
        "date",
        "hour",
        "percent"
      )
      .where("project_id", project_id)
      .orderBy("job.job_id", "desc")
  }

  create_my_leave_request({
    leaName_type, leaName_thai, max_day, id_company
  }) {

    return knex("lea_type")
      .insert({
        leaName_type: leaName_type,
        leaName_thai: leaName_thai,
        max_day: max_day,
        IDcompany: id_company,
      });
  }

  show_my_leave_request() {
    console.log("A");
    return knex("lea_type")
      .select(
        "leaID_type",
        "leaName_type",
        "leaName_thai",
        "max_day",
        "IDcompany"
      )
      .orderBy("lea_type.leaID_type", "desc");
  }
  insert_my_leave_request({
    lea_type,
    lea_day,
    date_start,
    date_end,
    lea_reason,
    empID,
    status,
    add_empID,
    date_created,
  }) {
    return knex("leave_request")
      .insert({
        lea_type: lea_type,
        lea_day: lea_day,
        date_start: date_start,
        date_end: date_end,
        lea_reason: lea_reason,
        empID: empID,
        status: status,
        add_empID: add_empID,
        date_created: date_created,
      });
  }

  create_my_flexible_request({
    benName_type, id_company
  }) {

    return knex("ben_type")
      .insert({
        benName_type: benName_type,
        IDcompany: id_company,
      });
  }

  show_my_flexible_request() {
    return knex("ben_type")
      .select(
        "benID_type",
        "benName_type",

      )
      .orderBy("ben_type.benID_type", "desc");
  }

  insert_my_flexible_benefit({
    ben_type,
    ben_date_request,
    ben_date_receipt,
    amount,
    empID,
    status,
    add_empID,
    flexible_created
  }) {
    return knex("benefit")
      .insert({
        ben_type: ben_type,
        ben_date_request: ben_date_request,
        ben_date_receipt: ben_date_receipt,
        amount: amount,
        empID: empID,
        status: status,
        add_empID: add_empID,
        flexible_created: flexible_created
      })
  }
  report_leave({ empID, date_start, date_end, date_s, date_e }) {
    console.log({ empID, date_start, date_end, date_s, date_e });

    return knex("leave_request")

      .select(
        "employee.empID",
        "leaID",
        "employee.emp_name",
        "employee.emp_last",
        "lea_type.leaName_type",
        "lea_day.leaName_day",
        "date_start",
        "date_end",
        "lea_reason",
        "status.sta_name",
        "date_created",
        "comname"
      )
      .leftJoin("lea_type", "leave_request.lea_type", "lea_type.leaID_type")
      .leftJoin("lea_day", "leave_request.lea_day", "lea_day.leaID_day")
      .leftJoin("status", "leave_request.status", "status.staID")
      .leftJoin("employee", "leave_request.empID", "employee.empID")
      .leftJoin("company", "employee.company", "company.comID")
      .columns(knex.raw("concat(employee.emp_name ,' ', employee.emp_last) as full_name"))
      .columns(knex.raw("datediff(leave_request.date_end,leave_request.date_start)+1 as total_day"))

      .columns(knex.raw("timediff(leave_request.date_end,leave_request.date_start)/10000 as time_day"))
      .orderBy("leave_request.leaID", "desc")
      .where('date_start', '>=', date_s)
      .andWhere('date_end', '<=', date_e)
      .andWhere('employee.empID', empID)

  }


  report_flexible({
    date_A1, date_B1
  }) {
    return knex("benefit")

      .select(
        "benID",
        "employee.emp_name",
        "employee.emp_last",
        "company.comname",
        "ben_type.benName_type",
        "ben_date_request",
        "ben_date_receipt",
        "amount",
        "status.sta_name",
        "employee.userName",
        "flexible_created",
        "employee.email"
      )
      .leftJoin("employee", "benefit.empID", "employee.empID")
      .leftJoin("company", "employee.company", "company.comID")
      .leftJoin("ben_type", "benefit.ben_type", "ben_type.benID_type")
      .leftJoin("status", "benefit.status", "status.staID")
      .columns(knex.raw("concat(employee.emp_name ,' ', employee.emp_last) as full_name"))
      .orderBy("benefit.benID", "desc")
      .whereBetween("flexible_created", [date_A1, date_B1])

  }

  show_quest() {
    return knex("project")
      .select(
        "project_name",
        "project_start",
        "project_end",
        "company_id",
        "user_create"

      )
      .orderBy("project.project_id", "desc")
  }
}
module.exports = new AuthModel();
