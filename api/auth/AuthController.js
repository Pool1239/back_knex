/* สําหรับสร้าง Controller */

const { success, failed } = require("../../config/response");
const { set_cookie, remove_cookie } = require("../../config/cookie_action");
const AuthModel = require("./AuthModel");
const bcrypt = require("bcrypt");
const moment = require("moment-timezone");
const fs = require("fs")
const { Parser } = require('json2csv');
const csv = require("fast-csv")
const ws = fs.createWriteStream("my.csv");
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const encoding = require("encoding");

var Papa = require("../papaparse.js");
// var assert = require('assert');
// var longSampleRawCsv = fs.readFileSync(__dirname + '/long-sample.csv', 'utf8');

class AuthController {
  /*คีย์ลัด cfn*/

  async login(req, res) {
    // console.log(req.body);
    try {
      const { username, password } = req.body;
      const userData = await AuthModel.selectPassword(username);
      const checkPassword = await bcrypt.compare(password, userData.password);
      console.log("id", userData.empID);
      if (checkPassword == true) {
        success(res, userData.empID);
      } else {
        failed(res, "Password ไม่ถูกต้อง");
      }
    } catch (error) {
      failed(res, "No user");
    }
  }
  logout(req, res) {
    remove_cookie(req);
    success(res, "logout success");
  }
  async profile(req, res) {
    try {
      // console.log(req.body);
      const { empID } = req.body;
      const userData = await AuthModel.selectProfile(empID);
      // console.log(userData);
      const data = {
        ...userData,
        starting_date: moment(userData.starting_date).format(
          "DD-MM-YYYY HH:mm"
        ),
        full_name: userData.emp_name + " " + userData.emp_last
      };
      // console.log(data);
      const years = await AuthModel.getEmpYear(empID);
      // console.log(years[0][0]);
      success(res, [
        {
          ...years[0][0],
          ...data
        }
      ]);
    } catch (error) {
      // console.log(error);
      failed(res, "fail");
    }
  }

  async re_password(req, res) {
    // console.log(req.body);
    try {
      const { email, password } = req.body;
      const con_email = await AuthModel.con_email(email);
      // console.log(con_email);
      if (con_email != undefined) {
        const en_password = await bcrypt.hash(password, 10);
        await AuthModel.re_password(en_password, email);
        success(res, "Success");
      } else {
        failed(res, "Email ไม่ถูกต้อง");

      }
    } catch (error) {
      console.log("error" + error);

      failed(res, error);
    }
  }

  async insert_user(req, res) {
    try {
      const {
        emp_name,
        emp_last,
        userName,
        role,
        email,
        pro_period,
        user_create_id,
        company
      } = req.body;
      const starting_date = moment
        .tz("Asia/Bangkok")
        .format("YYYY-MM-DD HH:mm:ss");
      const con_email = await AuthModel.con_email(email);
      if (con_email === undefined) {
        const con_username = await AuthModel.con_username(userName);
        if (con_username === undefined) {
          const position = " - ";
          const address = " - ";
          const tel = " - ";
          await AuthModel.insert_user(
            emp_name,
            emp_last,
            userName,
            role,
            email,
            pro_period,
            starting_date,
            user_create_id,
            company,
            position,
            address,
            tel
          );
          success(res, "Success");
        } else {
          success(res, "Username ถูกใช้ไปแล้ว");
        }
      } else {
        success(res, "Email ถูกใช้ไปแล้ว");
      }
    } catch (error) {
      // console.log(error);
      failed(res, "code error");
    }
  }
  async alldata_user(req, res) {
    try {
      const datas = await AuthModel.alldata_user();
      const datas2 = datas;
      const data3 = datas.length;
      success(res, { datas2, data3 });
    } catch (error) {
      // console.log(error);
      failed(res, "error");
    }
  }
  async delete_user(req, res) {
    try {
      const { empID } = req.body;
      // console.log(empID);
      await AuthModel.delete_user(empID);
      success(res, "Success");
    } catch (error) {
      console.log(error);
      failed(res, "");
    }
  }

  async update_user(req, res) {
    try {
      const { datas } = req.body;
      await Promise.all(
        datas.map(el =>
          AuthModel.update_user({
            empID: el.empID,
            emp_name: el.emp_name,
            emp_last: el.emp_last,
            email: el.email,
            pro_period: el.pro_name == "Pass" ? 1 : 2,
            role: el.rol_name == "Admin" ? 1 : el.rol_name == "Employee" ? 2 : 3
          })
        )
      );
      success(res, "Success");
    } catch (error) {
      // console.log(error);
      failed(res, "เกิดข้อผิดพลาด");
    }
  }
  async update_user_pagehome(req, res) {
    try {
      const { empID, address, emp_name, emp_last, email, tel } = req.body;
      // console.log(empID, address, emp_name, emp_last, email, tel);
      await AuthModel.update_user_pagehome(
        empID,
        address,
        emp_name,
        emp_last,
        email,
        tel
      );
      success(res, "Success");
    } catch (error) {
      // console.log(error);
      failed(res, "Warning แก้ไขไม่สำเร็จ");
    }
  }
  async alldata_company(req, res) {
    try {
      const data = await AuthModel.alldata_company();
      const data2 = data.length;
      success(res, { data, data2 });
    } catch (error) {
      failed(res, "No Data");
    }
  }
  async insert_company(req, res) {
    try {
      const { comname, time_in, time_out } = req.body;
      await AuthModel.insert_company(comname, time_in, time_out);
      success(res, "Success");
    } catch (error) {
      failed(res, "Error");
    }
  }
  async update_company(req, res) {
    try {
      const { datas } = req.body;
      await Promise.all(
        datas.map(el =>
          AuthModel.update_company({
            comID: el.comID,
            comname: el.comname,
            time_in: el.time_in,
            time_out: el.time_out
          })
        )
      );
      success(res, "Success");
    } catch (error) {
      console.log(error);
      failed(res, "เกิดข้อผิดพลาด");
    }
  }
  async delete_company(req, res) {
    try {
      const { comID } = req.body;
      // console.log(empID);
      await AuthModel.delete_company(comID);
      success(res, "Success");
    } catch (error) {
      // console.log(error);
      failed(res, "");
    }
  }
  async sum_maxday(req, res) {
    try {
      const { IDcompany } = req.body;
      const data = await AuthModel.sum_maxday(IDcompany);
      const data2 = data.map(el => el.max_day);
      const data3 = data2.reduce((partial_sum, a) => partial_sum + a);
      success(res, data3);
    } catch (error) {
      failed(res, "ไม่มีข้อมูล");
    }
  }
  async use_the_leave(req, res) {
    try {
      const { empID } = req.body;
      const lea_day1 = 1;
      const lea_day2 = 2;
      const data = await AuthModel.use_leave_day({ empID, lea_day: lea_day1 });
      const data2 = await AuthModel.use_leave_time({
        empID,
        lea_day: lea_day2
      });
      const data3 = data2.reduce(
        (acc, el) => acc + time2minute(el.sum_time),
        0
      );
      const data4 = minute2time(data3, data);
      success(res, { data4 });
    } catch (error) {
      console.log(error);
      failed(res, "ไม่มีข้อมูล");
    }
  }
  async requset(req, res) {
    try {
      const status1 = 1;
      const status2 = 2;
      const status3 = 3;
      const data1 = await AuthModel.request_wait({ status: status1 });
      const data2 = await AuthModel.request_approve({ status: status2 });
      const data3 = await AuthModel.request_reject({ status: status3 });
      success(res, {
        waits: data1[0].waits,
        approve: data2[0].approve,
        reject: data3[0].reject
      });
    } catch (error) {
      failed(res, "ไม่มีข้อมูล");
    }
  }


  async alldata_leave_request(req, res) {
    try {
      const datas = await AuthModel.alldata_leave_request();
      const datas2 = datas.map(el => {

        // let date_start = parseInt(moment(el.date_start).format('hh'))
        // let date_end = parseInt(moment(el.date_end).format('hh'))
        // let caldate = date_end - date_start
        // let date_start2 = parseInt(moment(el.date_start).format("DD"))
        // let date_end2 = parseInt(moment(el.date_end).format("DD"))
        // let calday = (date_end2 - date_start2) + 1
        // let time_day2 = moment(parseInt(el.time_day)).format("HH")
        // let total_time = caldate
        // let total_day = calday
        let totalall = el.leaName_day === "Full day" ? el.total_day + " " + "day" : parseInt(el.time_day) + " " + "hour"

        return {
          ...el, totalall
          // date_start, date_end, caldate, calday, totalall
        }
      })
      success(res, datas2);
    } catch (error) {
      // console.log(error);
      failed(res, error.message);

    }
  }

  async approve_status(req, res) {
    try {
      const { leaID } = req.body;
      await AuthModel.approve_status(leaID);
      console.log("leaID", leaID);
      success(res, leaID);
    } catch (error) {
      console.log(error);
      failed(res, "fail");
    }
  }

  async reject_status(req, res) {
    try {
      const { leaID } = req.body;
      await AuthModel.reject_status(leaID);
      console.log("leaID", leaID);
      success(res, leaID);
    } catch (error) {
      console.log(error);
      failed(res, "fail");
    }
  }

  async alldata_flexible_benefit(req, res) {
    try {
      const datas = await AuthModel.alldata_flexible_benefit();
      success(res, datas);
    } catch (error) {
      // console.log(error);
      failed(res, error.message);

    }
  }

  async approve_statusBen(req, res) {
    try {
      const { benID } = req.body;
      await AuthModel.approve_statusBen(benID);
      console.log("benID", benID);
      success(res, benID);
    } catch (error) {
      console.log(error);
      failed(res, "fail");
    }
  }

  async reject_statusBen(req, res) {
    try {
      const { benID } = req.body;
      await AuthModel.reject_statusBen(benID);
      console.log("benID", benID);
      success(res, benID);
    } catch (error) {
      console.log(error);
      failed(res, "fail");
    }
  }

  async alldata_work(req, res) {
    try {
      const datas = await AuthModel.alldata_work();
      success(res, datas);
    } catch (error) {
      // console.log(error);
      failed(res, error.message);

    }
  }

  async checkin(req, res) {
    try {
      // console.log(req.body);
      // const { empID } = req.body;
      const empID = { empID: req.body.empID }
      // console.log("A", empID);

      const datas = await AuthModel.select_for_checkin(empID);
      // console.log(datas[0]);
      const check_undefined = await AuthModel.check_undefined(empID);
      // console.log(check_undefined);
      if (check_undefined[0] === undefined) {
        const datas1 = await AuthModel.checkin(datas[0])
        success(res, "Goodmorning");
      } else {
        success(res, "You have already checked in");
      }


    } catch (error) {
      console.error(error)
      failed(res, "Error");
    }
  }

  async inbreak(req, res) {
    try {
      const { empID } = req.body
      console.log("A", empID);

      const check_undefined = await AuthModel.check_undefined({ empID });
      console.log("B1", check_undefined[0]);
      if (check_undefined[0] != undefined) {
        if (check_undefined[0].time_in_break === "00:00:00") {
          console.log("1");
          await AuthModel.inbreak(check_undefined[0]);
          success(res, "Enjoy your break");
        } else {
          console.log("1 2");
          success(res, "You have already time in break");
        }
      } else {
        console.log("0 0");

        console.log("empID", empID);
        const datas1 = await AuthModel.select_for_checkin({ empID });
        console.log("com", datas1[0].company);
        const company = datas1[0].company
        await AuthModel.checkin({ empID, company })
        const check_undefined = await AuthModel.check_undefined({ empID });
        await AuthModel.inbreak(check_undefined[0]);
        success(res, "You Late");
      }

    } catch (error) {
      console.error(error);
      failed(res, "Error")

    }
  }

  async outbreak(req, res) {

    try {
      const { empID } = req.body
      console.log("A", empID);

      const check_undefined = await AuthModel.check_undefined({ empID });
      console.log("B1", check_undefined[0]);
      if (check_undefined[0] != undefined) {
        if (check_undefined[0].time_in_break != "00:00:00") {
          if (check_undefined[0].time_out_break === "00:00:00") {
            console.log("1 2 0");
            await AuthModel.outbreak(check_undefined[0]);
            success(res, "Time for Working");
          } else {
            console.log("1 2 3");
            success(res, "You have already time out break");
          }
        } else {
          console.log("1 0 0");

          console.log("empID", empID);

          const check_undefined = await AuthModel.check_undefined({ empID });
          await AuthModel.inbreak(check_undefined[0]);
          await AuthModel.outbreak(check_undefined[0]);
          success(res, "Time for Working");
        }

      } else {
        console.log("0 0 0");

        console.log("empID", empID);
        const datas1 = await AuthModel.select_for_checkin({ empID });
        console.log("com", datas1[0].company);
        const company = datas1[0].company
        await AuthModel.checkin({ empID, company })
        const check_undefined = await AuthModel.check_undefined({ empID });
        await AuthModel.inbreak(check_undefined[0]);
        await AuthModel.outbreak(check_undefined[0]);
        success(res, "You Late");
      }

    } catch (error) {
      console.error(error);
      failed(res, "Error")

    }

    // try {
    //   const { empID } = req.body
    //   const date = moment().format("YYYY-MM-DD")
    //   // const date=moment().format("YYYY-MM-DD")
    //   console.log("A", empID);
    //   console.log("A1", date);

    //   const check_undefined = await AuthModel.check_undefined({ empID, date });
    //   console.log("B", check_undefined);
    //   // console.log("B2",check_undefined[0]);

    //   if (check_undefined.length != 0) {
    //     console.log("1");

    //     await AuthModel.outbreak(check_undefined[0]);
    //     success(res, "Welcome back");
    //   } else {
    //     console.log("empID", empID);
    //     console.log("date", date);
    //     const datas1 = await AuthModel.select_for_checkin({ empID });
    //     console.log("com", datas1[0].company);
    //     const company = datas1[0].company
    //     await AuthModel.checkin({ empID, date, company })
    //     const check_undefined = await AuthModel.check_undefined({ empID, date });
    //     await AuthModel.inbreak(check_undefined[0]);
    //     await AuthModel.outbreak(check_undefined[0]);
    //     success(res, "You Late");
    //   }

    // } catch (error) {
    //   console.error(error);
    //   failed(res, "Error")

    // }
  }

  async checkout(req, res) {

    try {
      const { empID } = req.body
      console.log("A", empID);

      const check_undefined = await AuthModel.check_undefined({ empID });
      console.log("B1", check_undefined[0]);
      if (check_undefined[0] != undefined) {
        if (check_undefined[0].time_in_break != "00:00:00") {
          if (check_undefined[0].time_out_break != "00:00:00") {
            if (check_undefined[0].date_out === "00:00:00") {
              console.log("1");
              await AuthModel.checkout(check_undefined[0]);
              success(res, "Get Home Safely");
            } else {
              console.log("2");
              success(res, "You have already check out");
            }
          } else {
            console.log("empID", empID);
            const check_undefined = await AuthModel.check_undefined({ empID });
            await AuthModel.outbreak(check_undefined[0]);
            await AuthModel.checkout(check_undefined[0]);
            success(res, "Get Home Safely");
          }
        } else {
          console.log("empID", empID);
          const check_undefined = await AuthModel.check_undefined({ empID });
          await AuthModel.inbreak(check_undefined[0]);
          await AuthModel.outbreak(check_undefined[0]);
          await AuthModel.checkout(check_undefined[0]);
          success(res, "Get Home Safely");
        }

      } else {
        console.log("3");

        console.log("empID", empID);
        const datas1 = await AuthModel.select_for_checkin({ empID });
        console.log("com", datas1[0].company);
        const company = datas1[0].company
        await AuthModel.checkin({ empID, company })
        const check_undefined = await AuthModel.check_undefined({ empID });
        await AuthModel.inbreak(check_undefined[0]);
        await AuthModel.outbreak(check_undefined[0]);
        await AuthModel.checkout(check_undefined[0]);
        success(res, "You Late");
      }

    } catch (error) {
      console.error(error);
      failed(res, "Error")

    }

    // try {
    //   const { empID } = req.body
    //   const date = moment().format("YYYY-MM-DD")
    //   // const date=moment().format("YYYY-MM-DD")
    //   console.log("A", empID);
    //   console.log("A1", date);

    //   const check_undefined = await AuthModel.check_undefined({ empID, date });
    //   console.log("B", check_undefined);
    //   // console.log("B2",check_undefined[0]);

    //   if (check_undefined.length != 0) {
    //     console.log("1");

    //     await AuthModel.checkout(check_undefined[0]);
    //     success(res, "Get home safely");
    //   } else {
    //     console.log("empID", empID);
    //     console.log("date", date);
    //     const datas1 = await AuthModel.select_for_checkin({ empID });
    //     console.log("com", datas1[0].company);
    //     const company = datas1[0].company
    //     await AuthModel.checkin({ empID, date, company })
    //     const check_undefined = await AuthModel.check_undefined({ empID, date });
    //     await AuthModel.checkout(check_undefined[0]);
    //     success(res, "Yeah Goodbye but you so late");
    //   }

    // } catch (error) {
    //   console.error(error);
    //   failed(res, "Error")

    // }
  }
  async chartw(req, res) {
    try {
      const { empID, month, year } = req.body
      // console.log("empID",empID);
      console.log("month1", month);
      // console.log("year",year);
      if (month.length == 1) {
        const month1 = "0" + month
        if (empID.length != 0 && month.length != 0 && year.length != 0) {
          const monthc = await AuthModel.monthchart({ empID, month: month1, year })
          const monthc1 = monthc.map(el => {

            let Date = moment(el.date).format("DD-MM-YYYY")

            return {
              ...el, Date
            }
          })
          success(res, monthc1)
        } else if (empID.length != 0 && month.length == 0 && year.length != 0) {
          const yearc = await AuthModel.yearchart({ empID, month: month1, year })
          const yearc1 = yearc.map(el => {

            let Date = moment(el.date).format("YYYY-MM-DD")

            return {
              ...el, Date
            }
          })
          success(res, yearc1)
        }
      } else {
        const month1 = month
        if (empID.length != 0 && month.length != 0 && year.length != 0) {
          const monthc = await AuthModel.monthchart({ empID, month: month1, year })
          const monthc1 = monthc.map(el => {

            let Date = moment(el.date).format("YYYY-MM-DD")
            let Hour = moment(el.total_work).format("HH")
            let Minute = moment(el.total_work).format("mm")


            return {
              ...el, Date, Hour, Minute
            }
          })


          success(res, monthc1)
        } else if (empID.length != 0 && month.length == 0 && year.length != 0) {
          const yearc = await AuthModel.yearchart({ empID, month: month1, year })
          const yearc1 = yearc.map(el => {

            let Date = moment(el.date).format("YYYY-MM-DD")

            return {
              ...el, Date
            }
          })
          success(res, yearc1)
        }
      }


    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }
  async alldata_project(req, res) {
    try {
      const datas = await AuthModel.alldata_project();
      const datas2 = datas.length
      success(res, { datas, datas2 });
    } catch (error) {
      // console.log(error);
      failed(res, error.message);

    }
  }
  async insert_project(req, res) {
    try {

      const { empID } = req.body
      console.log("empID", empID);

      const select_company = await AuthModel.select_company({ empID })
      console.log("com", select_company[0].company);
      const id_company = select_company[0].company
      const {
        project_name,
        project_start,
        project_end
      } = req.body
      console.log("project_name", project_name);
      console.log("project_start", project_start);
      console.log("project_end", project_end);


      const insert_project = await AuthModel.insert_project({
        project_name,
        project_start,
        project_end,
        id_company,
        empID,
      })
      success(res, insert_project)
    } catch (error) {
      console.error(error);
      failed(res, "Error")

    }
  }
  async delete_project(req, res) {
    try {
      const { project_id } = req.body
      // console.log("project_id",project_id);

      await AuthModel.delete_project({ project_id })
      success(res, "Complete")
    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }
  async update_project(req, res) {
    try {
      // const { project_id, project_name } = req.body
      const { datas } = req.body;
      console.log("datas", datas[0].project_id);
      await Promise.all(
        datas.map(el =>
          AuthModel.update_project({
            // comname: el.conname,
            // company_id: el.company_id,
            // emp_last: el.emp_last,
            // emp_name: el.emp_name,
            // find: el.find,
            // full_name: el.full_name,
            // project_end: el.project_end,
            project_id: el.project_id,
            project_name: el.project_name,
            // project_start: el.project_start,
            // user_create: el.user_create
          })
        )
      );
      // const update_project = await AuthModel.update_project({ project_id, project_name })
      success(res, "Complete")
    } catch (error) {
      console.error(error);
      failed(res, "Error")

    }
  }

  async chartp(req, res) {
    try {
      const { project_id } = req.body
      const chart_project = await AuthModel.chart_project({ project_id })
      const chart_project1 = chart_project.map(el => {
        let Date = moment(el.date).format("DD-MM-YYYY")
        return {
          ...el, Date
        }
      })
      success(res, chart_project1)
    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }
  async create_my_leave_request(req, res) {
    try {
      const { empID } = req.body
      console.log("empID", empID);
      const select_company = await AuthModel.select_company({ empID })
      console.log("com", select_company[0].company);
      const id_company = select_company[0].company
      const { leaName_type, leaName_thai, max_day } = req.body
      const create_my_leave_request = await AuthModel.create_my_leave_request({ leaName_type, leaName_thai, max_day, id_company })
      success(res, "Complete")
    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }

  async show_my_leave_request(req, res) {
    try {

      const show_my_leave_request = await AuthModel.show_my_leave_request()
      success(res, show_my_leave_request)
    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }

  async insert_my_leave_request(req, res) {
    try {
      const {
        leaID_type,
        leaID_day,
        date_start,
        date_end,
        lea_reason,
        empID,
      } = req.body
      const status = "1"
      const add_empID = "17"
      const date_created = moment().format("YYYY-MM-DD HH:mm:ss")
      await AuthModel.insert_my_leave_request({
        lea_type: leaID_type,
        lea_day: leaID_day,
        date_start,
        date_end,
        lea_reason,
        empID,
        status,
        add_empID,
        date_created,
      })
      success(res, "Complete")
    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }

  async create_my_flexible_request(req, res) {
    try {
      const { empID } = req.body
      console.log("empID", empID);
      const select_company = await AuthModel.select_company({ empID })
      console.log("com", select_company[0].company);
      const id_company = select_company[0].company
      const { benName_type } = req.body
      console.log("BT", { empID, id_company, benName_type });

      const create_my_flexible_request = await AuthModel.create_my_flexible_request({ benName_type, id_company })
      success(res, "Complete")
    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }

  async show_my_flexible_request(req, res) {
    try {
      const datas = await AuthModel.show_my_flexible_request()
      success(res, datas)
    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }

  async insert_my_flexible_benefit(req, res) {
    try {
      const {
        ben_type,
        ben_date_receipt,
        amount,
        empID,
        files
      } = req.body
      // console.log("IN", 
      //   ben_type,
      //  );

      const status = "1"
      const add_empID = "17"
      const ben_date_request = moment().format("YYYY-MM-DD HH:mm:ss")
      const flexible_created = moment().format("YYYY-MM-DD HH:mm:ss")
      const A = await AuthModel.insert_my_flexible_benefit({
        ben_type,
        ben_date_request,
        ben_date_receipt,
        amount,
        empID,
        status,
        add_empID,
        flexible_created,
      })
      console.log("A", A[0]);
      const base64 = files.split(',')[1]
      require("fs").writeFile("./img/" + A[0] + ".png", base64, "base64", function (err) {
        success(res, "Complete")
      })


    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }
  async show_img(req, res) {
    try {
      // const{benID}=req.body
      console.log(__dirname.replace("api", "").replace("auth", "") + "/img/" + req.params.benID + ".png");

      fs.readFile(__dirname.replace("api", "").replace("auth", "") + "/img/" + req.params.benID + ".png", (err, data) => {
        err
          ? res.sendFile(__dirname.replace("api", "").replace("auth", "") + "/img/default.png")
          : res.sendFile(__dirname.replace("api", "").replace("auth", "") + "/img/" + req.params.benID + ".png");
      });


    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }
  async show_image(req, res) {
    try {
      const { benID } = req.body
      console.log(__dirname.replace("api", "").replace("auth", "") + "/img/" + benID + ".png");

      fs.readFile(__dirname.replace("api", "").replace("auth", "") + "/img/" + benID + ".png", (err, data) => {
        err
          ? res.sendFile(__dirname.replace("api", "").replace("auth", "") + "/img/default.png")
          : res.sendFile(__dirname.replace("api", "").replace("auth", "") + "/img/" + benID + ".png");
      });


    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }
  async report_leave(req, res) {
    try {

      const { date_start, date_end, empID } = req.body
      const date_s = moment(date_start).format("YYYY-MM-DD 00:00:00")
      const date_e = moment(date_end).format("YYYY-MM-DD 23:59:59")
      // console.log("A", date_s);

      const report_leave = await AuthModel.report_leave({ date_start, date_end, date_s, date_e, empID })
      const report_leave2 = report_leave.map(el => {

        let totalall = el.leaName_day === "Full day" ? el.total_day + " " + "day" : parseInt(el.time_day) + " " + "hour"
        let start = moment(el.date_start).format("DD-MM-YYYY")
        let end = moment(el.date_end).format("DD-MM-YYYY")
        let created = moment(el.date_created).format("DD-MM-YYYY")

        let leaID = el.leaID
        let empID = el.empID
        let full_name = el.full_name
        let leaName_type = el.leaName_type
        let leaName_day = el.leaName_day
        let lea_reason = el.lea_reason
        let sta_name = el.sta_name

        
        return {
          leaID, empID, full_name, leaName_type, leaName_day, start, end, lea_reason, sta_name, created, totalall
        }
      })
      console.log(report_leave2);
    
      const csvWriter = createCsvWriter({
        path: 'out.csv',
        header: [
          { id: 'leaID', title: 'LeaveID' },
          { id: 'empID', title: 'EmployeeID' },
          { id: 'full_name', title: 'FullName' },
          { id: 'leaName_type', title: 'Type' },
          { id: 'leaName_day', title: 'Day' },
          { id: 'start', title: 'Start' },
          { id: 'end', title: 'End' },
          { id: 'lea_reason', title: 'Reason' },
          { id: 'sta_name', title: 'Status' },
          { id: 'created', title: 'Created' },
          { id: 'totalall', title: 'Conclude' },
        ]
      });
      const data = report_leave2;
      csvWriter
        .writeRecords(data)
        .then(() => console.log('The CSV file was written successfully'));
      success(res, report_leave2)
    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }
  async report_flexible(req, res) {
    try {
      const {empID, date_A, date_B } = req.body
      const date_A1 = moment(date_A).format("YYYY-MM-DD 00:00:00")
      const date_B1 = moment(date_B).format("YYYY-MM-DD 23:59:59")
      const report_flexible = await AuthModel.report_flexible({ date_A1, date_B1 })
      success(res, report_flexible)
    } catch (error) {
      console.error(error);
      failed(res, "Error")

    }
  }

  async show_quest(req, res) {
    try {
      const show_quest = await AuthModel.show_quest()
      success(res, show_quest)
    } catch (error) {
      console.error(error);
      failed(res, "Error")
    }
  }
  // async update_company(req, res) {
  //   try {
  //     const { datas } = req.body;
  //     await Promise.all(
  //       datas.map(el =>
  //         AuthModel.update_company({
  //           comID: el.comID,
  //           comname: el.comname,
  //           time_in: el.time_in,
  //           time_out: el.time_out
  //         })
  //       )
  //     );
  //     success(res, "Success");
  //   } catch (error) {
  //     console.log(error);
  //     failed(res, "เกิดข้อผิดพลาด");
  //   }
  // }

}

const iden = fn => arg => fn(arg);

function time2minute(time = "") {
  const [hour, minute] = time.split(":").map(iden(parseInt));
  return hour * 60 + minute;
}

function minute2time(minute = 0, sum_date = 0) {
  const hour = parseInt(minute / 60);
  const hour1 = hour % 24;
  const hour2 = parseInt(hour / 24);
  const minutes = minute % 60;
  const day1 = parseInt(sum_date[0].sum_date);
  const day = day1 + hour2;
  const time = is10up(hour1) + ":" + is10up(minutes);
  return { time, day };
}

const is10up = val => (val < 10 ? "0" + val : val);

module.exports = new AuthController();
