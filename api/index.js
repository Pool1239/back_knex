const auth = require('./auth/AuthRoute')
// const leave_request = require('./leave_request/LeaveRequestRoute')

module.exports = (app, version) => {
  app.use(version + '/auth', auth)
  // app.use(version + '/leave_request', leave_request)
}
